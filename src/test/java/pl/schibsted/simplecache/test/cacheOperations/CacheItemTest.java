package pl.schibsted.simplecache.test.cacheOperations;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import pl.schibsted.simplecache.entity.CacheItem;

public class CacheItemTest {

	@Spy
	CacheItem item;
	
	
	@Test
	public void whenExpirationDateIsPast_shouldBeExpiredItem() {
		//given
		Date now = new Date();
		Date pastDate = new Date(now.getTime() - 1L);
		int notImportantTTL = -1;
		item = new CacheItem("key", "content".getBytes(), notImportantTTL);
		MockitoAnnotations.initMocks(this);
		//when
		when(item.getExpirationDate()).thenReturn(pastDate);
		//then
		assertThat(item.isExpired()).isTrue();
	}
}
