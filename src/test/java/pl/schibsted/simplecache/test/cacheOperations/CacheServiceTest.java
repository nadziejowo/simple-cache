package pl.schibsted.simplecache.test.cacheOperations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import pl.schibsted.simplecache.exception.NotExistingKeyException;
import pl.schibsted.simplecache.repository.CacheEngine;
import pl.schibsted.simplecache.service.CacheService;

public class CacheServiceTest {
	
	private static final String DEFAULT_KEY = "xxx";


	@InjectMocks
	CacheService cacheService;
	
	@Mock
	CacheEngine cacheEngine;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = NotExistingKeyException.class)
	public void whenDeleteNotExistingKey_shouldThrowNotExistingKeyException() throws NotExistingKeyException {
		//given
		when(cacheEngine.delete(DEFAULT_KEY)).thenReturn(null);
		//when
		cacheService.delete(DEFAULT_KEY);
	}
	
	@Test
	public void whenDeleteAllCache_shouldCacheBeDeleted() {
		//when
		cacheService.deleteAll();
		//then
		verify(cacheEngine, times(1)).clear();
		
	}
	
}
