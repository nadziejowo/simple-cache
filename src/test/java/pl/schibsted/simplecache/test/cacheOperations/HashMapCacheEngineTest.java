package pl.schibsted.simplecache.test.cacheOperations;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.repository.HashMapCacheEngine;


public class HashMapCacheEngineTest {

	private static final int AUTO_CLEAN_FREAQUENCY_IN_SEC = 1;

	@InjectMocks
	private HashMapCacheEngine cacheEngine = new HashMapCacheEngine(AUTO_CLEAN_FREAQUENCY_IN_SEC);
	
	@Spy
	private Map<String, CacheItem> cacheMap = new ConcurrentHashMap<>();
	
	private final byte[] DEFAULT_CONTENT = "xxxx".getBytes();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void whenAskingForExpiredItem_shouldReturnNull() {
		//given
		String key = "item key";
		CacheItem expiredItem = new CacheItem(key, "xxx".getBytes(), -12);
		when(cacheMap.getOrDefault(key, null)).thenReturn(expiredItem);
		//when
		assertThat(cacheEngine.findOne(key))
		//then
		.isNull();
	}	

	@Test
	public void whenAskingForValidItem_shouldReturnItem() {
		//given
		String key = "item key";
		int ttlInTheFuture = Integer.MAX_VALUE;
		CacheItem validItem = new CacheItem(key, DEFAULT_CONTENT, ttlInTheFuture);
		when(cacheMap.getOrDefault(key, null)).thenReturn(validItem);
		//when
		assertThat(cacheEngine.findOne(key))
		//then
		.isEqualTo(validItem);
	}	
	
	@Test
	public void whenSaveValidItem_shouldPutToMap() {
		//given
		String key = "item key";
		int ttlInTheFuture = Integer.MAX_VALUE;
		CacheItem validItem = new CacheItem(key, DEFAULT_CONTENT, ttlInTheFuture);
		//when 
		cacheEngine.save(validItem);
		//then
		verify(cacheMap, Mockito.times(1)).put(key, validItem);
	}

	@Test
	public void whenSaveInvalidItem_shouldPutToMap() {
		//given
		String key = "item key";
		int ttlInThePast = -12;
		CacheItem validItem = new CacheItem(key, DEFAULT_CONTENT, ttlInThePast);
		//when 
		cacheEngine.save(validItem);
		//then
		verify(cacheMap, Mockito.times(1)).put(key, validItem);
	}
	
	@Test
	public void whenCacheHaveValidItems_shouldBeenAvailable() {
		//given
		List<CacheItem> items = new ArrayList<>();
		int[] times = new int[]{1000, 4000, 5000};
		String keyPrefix = "key_";
		
		for(int time : times) {
			int ttl = time;
			CacheItem item = new CacheItem(keyPrefix + ttl, DEFAULT_CONTENT, ttl);
			items.add(item);
			cacheEngine.save(item);
		}
		
		int founded = 0;
		//when
		for(int time : times) {
			founded += cacheEngine.findOne(keyPrefix + time) != null ? 1 : 0;
		}
	
		//then
		assertThat(founded).isEqualTo(times.length);
	}

	
	@Test
	public void whenCacheItemsExpire_shouldBeDeleted() throws InterruptedException {
		//given
		List<CacheItem> items = new ArrayList<>();
		String keyPrefix = "key_";
		int[] times = new int[]{0, 1};
		
		for(int ttl : times) {
			CacheItem item = new CacheItem(keyPrefix + ttl, DEFAULT_CONTENT, ttl);
			items.add(item);
			cacheEngine.save(item);
		}
		//when
		Thread.sleep(3000L);
		//then
		InOrder order = inOrder(cacheMap);
		for(int ttl : times) {
			order.verify(cacheMap, Mockito.times(1)).remove(keyPrefix + ttl);
		}
	}

	@Test
	public void whenInCacheExpiredAndValidItems_shouldBeDeletedOnlyExpired() throws InterruptedException {
		//given
		String keyPrefix = "key_";
		int[] times = new int[]{0, 100, 1, 200};
		
		for(int ttl : times) {
			cacheEngine.save(new CacheItem(keyPrefix + ttl, DEFAULT_CONTENT, ttl));
		}
		//when
		Thread.sleep(3000L);
		//then
		InOrder order = inOrder(cacheMap);
		order.verify(cacheMap, Mockito.times(1)).remove(keyPrefix + 0);
		order.verify(cacheMap, Mockito.times(1)).remove(keyPrefix + 1);
		
		verify(cacheMap, Mockito.times(0)).remove(keyPrefix + 100);
		verify(cacheMap, Mockito.times(0)).remove(keyPrefix + 200);
	}
	
	@Test
	public void whenCacheCleaned_shouldBeEmpty() {
		
		//given
		int itemCount = 5;
		for(int i = 0; i < itemCount; i++) {
			String key = "" + i;
			CacheItem item = new CacheItem(key, DEFAULT_CONTENT, Integer.MAX_VALUE);
			cacheMap.put(key, item);
		}
		//when
		cacheEngine.clear();
		//then
		verify(cacheMap, times(1)).clear();
		for(int i = 0; i < itemCount; i++) {
			assertThat(cacheMap.get("" + i)).isNull();
		}
	}
}
