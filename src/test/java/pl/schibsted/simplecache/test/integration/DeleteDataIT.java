package pl.schibsted.simplecache.test.integration;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import pl.schibsted.simplecache.Application;
import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.repository.CacheEngine;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=Application.class)
@WebAppConfiguration
public class DeleteDataIT {

	private static final String BASE_URL = "/cache/";
	
	private final byte[] defaultContent = "xxxx".getBytes();
	private final String defaultKey = "xxxx";
	private final int veryLongTTL = Integer.MAX_VALUE;
	
	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	@Autowired
	private CacheEngine cacheEngine;
	
	@Before
	public void setUp() {
		mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void whenDeleteCachedItem_shouldReturnOkCode() throws Exception {
		//given
		CacheItem cachedItem = new CacheItem(defaultKey, defaultContent, veryLongTTL);
		cacheEngine.save(cachedItem);
		//when
		mockMvc.perform(delete(BASE_URL + defaultKey))
		//then
		.andExpect(status().isOk());
	}
	
	@Test
	public void whenDeleteNotExistingItem_shouldReturnNotFoundCode() throws Exception {
		//given
		String notCachedKey = defaultKey;
		//when
		mockMvc.perform(delete(BASE_URL + notCachedKey))
		//then
		.andExpect(status().isNotFound());
		
	}

	@Test
	public void whenDeleteAllItems_shouldDeleteCache() throws Exception {
		
		//given
		int cacheItemsCount = 10;
		CacheItem[] cacheItems = createCacheItems(cacheItemsCount);
		for(CacheItem item : cacheItems) {
			cacheEngine.save(item);
		}
		//when
		mockMvc.perform(delete("/cache"))
		//then
		.andExpect(status().isOk());
		for(CacheItem item : cacheItems) {
			assertThat(cacheEngine.findOne(item.getKey())).isNull();
		}		
	}

	private CacheItem[] createCacheItems(int cacheItemsCount) {
		CacheItem[] items = new CacheItem[cacheItemsCount];
		for(int i = 0; i < items.length; i++) {
			items[i] = new CacheItem(defaultKey + i, defaultContent, veryLongTTL);
		}
		return items;
	}
	
}
