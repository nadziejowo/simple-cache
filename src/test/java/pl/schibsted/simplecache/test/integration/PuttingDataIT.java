package pl.schibsted.simplecache.test.integration;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import pl.schibsted.simplecache.Application;
import pl.schibsted.simplecache.controller.ItemTags;
import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.repository.CacheEngine;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=Application.class)
@WebAppConfiguration
public class PuttingDataIT implements ItemTags {

	private static final String BASE_URL = "/cache/";
	private final byte[] defaultContent = "xxxx".getBytes();
	private final String defaultKey = "xxxx";
	private final int defaultTTL = Integer.MAX_VALUE;
	
	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	@Autowired
	private CacheEngine cacheEngine;
	
	@Value("${upload.file.max.kb}")
	private int maxUploadKB;
	
	@Before
	public void setUp() {
		mockMvc = webAppContextSetup(webApplicationContext).build();
	}
	
	
	@Test
	public void whenPutNewKeyWithItem_shouldReturnCreatedCode() throws Exception {
		//given
		String key = "new key";
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + key).content(defaultContent))
		//then
		.andExpect(status().isCreated());
	}
	
	
	@Test 
	public void whenPutExpiredItem_shoudReturnUnprocessableEntityCode() throws Exception {
		
		//given
		long expiredTTL = 0;
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + defaultKey).content(defaultContent)
				.param(ItemTags.TTL, String.valueOf(expiredTTL)))
		//then
		.andExpect(status().isUnprocessableEntity());
	}
	
	
	@Test
	public void whenPutAlreadyCachedKey_shouldReturnCreatedCode() throws Exception {
		//given
		CacheItem validItem = new CacheItem(defaultKey, defaultContent, defaultTTL);
		cacheEngine.save(validItem);
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + defaultKey).content(defaultContent)
				.param(ItemTags.TTL, String.valueOf(defaultTTL)))
		//then
		.andExpect(status().isCreated());
	}

	
	@Test
	public void whenPutEmptyItem_shouldReturnBadRequestCode() throws Exception {
		//given
		byte[] emptyItem = new byte[0];
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + defaultKey).content(emptyItem))
		//then
		.andExpect(status().isBadRequest());
	}

	
	@Test
	public void whenPutEmptyKey_shouldReturnMethodNotAllowedCode() throws Exception {
		//given
		String emptyKey = "";
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + emptyKey).content(defaultContent))
		//then
		.andExpect(status().isMethodNotAllowed());
	}
	
	
	@Test
	public void whenPutTooLargeItem_shouldReturnPayloadTooLargeCode() throws Exception {
		//given
		int tooLargeSize = maxUploadKB * 1000 + 1;
		byte[] tooLargeContent = new byte[tooLargeSize];
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + defaultKey).content(tooLargeContent))
		//then
		.andExpect(status().isPayloadTooLarge());
	}
	
	
	@Test
	public void whenPutAcceptableSizeItem_shouldReturnCreatedCode() throws Exception {
		//given
		int acceptableSize = maxUploadKB * 1000 - 1;
		byte[] acceptableSizeContent = new byte[acceptableSize];
		//when
		mockMvc.perform(MockMvcRequestBuilders.put(BASE_URL + defaultKey).content(acceptableSizeContent))
		//then
		.andExpect(status().isCreated());
	}
	
	
}
