package pl.schibsted.simplecache.test.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import pl.schibsted.simplecache.Application;
import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.repository.CacheEngine;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class GettingDataIT {

	private static final String BASE_URL = "/cache/";

	@Autowired
	private CacheEngine cacheEngine;
	
	private MockMvc mockMvc;
	@Autowired
	private WebApplicationContext webApplicationContext;

	private final byte[] DEFAULT_CONTENT = "xxxx".getBytes();

	@Before
	public void setUp() {
		mockMvc = webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void whenAskingForNotExistingKey_shouldReturnNotFoundCode() throws Exception {
		// given
		String key = "not existing key";
		// when
		mockMvc.perform(get(BASE_URL + key))
		// then
		.andExpect(status().isNotFound());
	}

	
		
	@Test
	public void whenAskingForExistingKey_shouldReturnCachedItemAndOkCode() throws Exception {
		// given
		String key = "existing key";
		int veryLongTTL = Integer.MAX_VALUE;
		CacheItem validItem = new CacheItem(key, DEFAULT_CONTENT, veryLongTTL);
		cacheEngine.save(validItem);
		// when
		mockMvc.perform(get(BASE_URL + key))
		// then
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.content().bytes(DEFAULT_CONTENT));
	}
	

	@Test
	public void whenAskingForExpiredItem_shouldReturnNotFoundCode() throws Exception {
		//given
		String key = "some key";
		int ttlForPast = -10;
		CacheItem expiredItem = new CacheItem(key, DEFAULT_CONTENT, ttlForPast);
		cacheEngine.save(expiredItem);
		//when
		mockMvc.perform(get(BASE_URL + key))
		//then
		.andExpect(status().isNotFound());
	}
}
