package pl.schibsted.simplecache.exception;

public class NotExistingKeyException extends Exception {

	public NotExistingKeyException(String itemKey) {
		super("Item with key: " + itemKey + " does not exist in the cache");
	}

	private static final long serialVersionUID = 1L;

	
}
