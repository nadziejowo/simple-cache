package pl.schibsted.simplecache.exception;

public class TooLargeItemException extends Exception {

	public TooLargeItemException(String itemKey, int maxSize) {
		super("The item with key " + itemKey + " exceeded max valid size: " + maxSize + "KB");
	}

	private static final long serialVersionUID = 1L;

}
