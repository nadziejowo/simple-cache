package pl.schibsted.simplecache.exception;

public class ExpiredItemException extends Exception {

	public ExpiredItemException(String key) {
		super("Item " + key + " is expired");
	}

	private static final long serialVersionUID = 1L;
	
	

}
