package pl.schibsted.simplecache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.exception.NotExistingKeyException;
import pl.schibsted.simplecache.repository.CacheEngine;

@Service
public class CacheService {

	@Autowired 
	private CacheEngine cacheEngine;
	
	public CacheItem find(String itemKey) {
		return cacheEngine.findOne(itemKey);
	}

	public void save(CacheItem item) {
		cacheEngine.save(item);
		
	}

	public void delete(String itemKey) throws NotExistingKeyException {
		if(cacheEngine.delete(itemKey) == null) {
			throw new NotExistingKeyException(itemKey);
		}
	}

	public void deleteAll() {
		cacheEngine.clear();
	}

	
	
}
