package pl.schibsted.simplecache.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pl.schibsted.simplecache.entity.CacheItem;
import pl.schibsted.simplecache.exception.ExpiredItemException;
import pl.schibsted.simplecache.exception.NotExistingKeyException;
import pl.schibsted.simplecache.exception.TooLargeItemException;
import pl.schibsted.simplecache.service.CacheService;

@RestController
@RequestMapping("/cache/{itemKey}")
public class CacheItemController {

	@Autowired
	private CacheService cacheService;
	
	@Value("${upload.file.max.kb}")
	private int uploadMaxSizeKB;
	
	
	/**
	 * 
	 * @param itemKey string representation of key
	 * @param content binary data to cache
	 * @param timeToLive time in seconds from now in which item will be kept in the cache
	 * @return json with short result of this operation explanation. 
	 * 			UNPROCESSABLE_ENTITY response code if timeToLive is less than 0 return.  
	 * 			PAYLOAD_TOO_LARGE response code if binary item is bigger than configured size  
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	public Answer putData(@PathVariable String itemKey, 
			@RequestBody byte[] content,
			@RequestParam(value = ItemTags.TTL) Optional<Integer> timeToLive) 
					throws ExpiredItemException, TooLargeItemException{
		
		
		int timeToLiveValue = timeToLive.orElse(Integer.MAX_VALUE);
		if(timeToLiveValue <= 0L) {
			throw new ExpiredItemException(itemKey);
		}
		
		if(isTooLargeItem(content)) {
			throw new TooLargeItemException(itemKey, uploadMaxSizeKB);
		}

		CacheItem item = new CacheItem(itemKey, content, timeToLiveValue);
		
		cacheService.save(item);
		return new Answer("Item " + itemKey + " cached.");
	}

	
	private boolean isTooLargeItem(byte[] content) {
		if(content.length > uploadMaxSizeKB * 1000) {
			return true;
		}
		return false;
	}

	/**
	 * @param itemKey string representation of key
	 * @return requested item from the cache or NOT_FOUND response code if no valid item 
	 * 			with that key is found in the cache.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public byte[] getData(@PathVariable String itemKey) throws NotExistingKeyException {
		CacheItem item = cacheService.find(itemKey);
		
		if(item == null){
			throw new NotExistingKeyException(itemKey);
		}
		return item.getContent();
	}

	
	/**
	 * @param itemKey string representation of key
	 * @return requested item from the cache or NOT_FOUND response code if no valid item 
	 * 			with that key is found in the cache.
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public Answer deleteData(@PathVariable String itemKey) throws NotExistingKeyException {
		cacheService.delete(itemKey);
		return new Answer("Deleted item with key: " + itemKey);
	}

	
	
	@ExceptionHandler(value = NotExistingKeyException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Answer notExistingKey(HttpServletRequest req, Exception e) {
		return new Answer(e.getMessage());
	}
	
	
	@ExceptionHandler(value = ExpiredItemException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public Answer expiredItem(HttpServletRequest req, Exception e) {
		return new Answer(e.getMessage());
	}

	
	@ExceptionHandler(value = TooLargeItemException.class)
	@ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
	public Answer payloadTooLarge(HttpServletRequest req, Exception e) {
		return new Answer(e.getMessage());
	}

	@ExceptionHandler(value = HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Answer emptyItem(HttpServletRequest req, Exception e) {
		return new Answer(e.getMessage());
	}
	
}

