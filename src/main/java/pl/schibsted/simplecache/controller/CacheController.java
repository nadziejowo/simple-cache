package pl.schibsted.simplecache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.schibsted.simplecache.service.CacheService;

@RestController
@RequestMapping("/cache")
public class CacheController {

	@Autowired
	private CacheService cacheService;
	
	@RequestMapping(method = RequestMethod.DELETE)
	public Answer delete() {
		
		cacheService.deleteAll();
		return new Answer("Deleted all items");
	}
}
