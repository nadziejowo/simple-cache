package pl.schibsted.simplecache.repository;

import pl.schibsted.simplecache.entity.CacheItem;

public interface CacheEngine {

	CacheItem findOne(String itemKey);

	void save(CacheItem item);

	CacheItem delete(String itemKey);

	void clear();

}
