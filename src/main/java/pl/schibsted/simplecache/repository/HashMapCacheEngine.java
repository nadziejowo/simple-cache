package pl.schibsted.simplecache.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import pl.schibsted.simplecache.entity.CacheItem;

@Component
public class HashMapCacheEngine implements CacheEngine {

	private Map<String, CacheItem> cacheMap = new ConcurrentHashMap<>();
	private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
	
	Runnable autoCleaningTask = new Runnable() {

		@Override
		public void run() {
			cacheMap.forEach((key, cachedItem) -> { 
				if(cachedItem.isExpired()){
					delete(key);
				}});
		}
	};
	

	@Autowired
	public HashMapCacheEngine(@Value("${autocleaning.period}") int autoCleaningFrequencyInSec) {
		executorService.scheduleAtFixedRate(autoCleaningTask, 1, autoCleaningFrequencyInSec, 
				TimeUnit.SECONDS);
	}
	
	@Override
	public CacheItem findOne(String itemKey) {
		
		 CacheItem cacheItem = cacheMap.getOrDefault(itemKey, null);
		 if(cacheItem == null || cacheItem.isExpired()) {
			 return null;
		 }
		 return cacheItem;
	}


	@Override
	public void save(CacheItem item) {
		cacheMap.put(item.getKey(), item);
	}


	@Override
	public CacheItem delete(String itemKey) {
		return cacheMap.remove(itemKey);
	}

	@Override
	public void clear() {
		cacheMap.clear();
	}

}
