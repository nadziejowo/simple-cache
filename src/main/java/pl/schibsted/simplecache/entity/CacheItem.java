package pl.schibsted.simplecache.entity;

import java.util.Date;

public class CacheItem {

	private byte[] content;
	private String key;
	private Date created;
	private Date exipirationDate;

	public CacheItem(String key, byte[] content, int ttlValue) {
		this.content = content;
		this.key = key;
		this.created = new Date();
		this.exipirationDate = new Date(created.getTime() + ttlValue * 1000L);
	}

	public String getKey() {
		return key;
	}

	public byte[] getContent() {
		return content;
	}

	public Date getExpirationDate() {
		return exipirationDate;
	}

	public boolean isExpired() {
		if(getExpirationDate().before(new Date())) {
			return true;
		}
		return false;
	}
}
